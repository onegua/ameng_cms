<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('welcome');
//});

//Route::get('/', [App\Http\Controllers\WelcomeController::class, 'index']);



//Route::get('testage/{age}',[App\Http\Controllers\WelcomeController::class, 'index'])->middleware('age');





Route::get('/', function () {
//    echo 3;exit;
    return \App::call(
        [App\Http\Controllers\Index\IndexController::class, 'index']);
});

Route::get('/admin', [App\Http\Controllers\Admin\MainController::class, 'index']);

//lm 路由中间件
//Route::get('/admin/{class}/{action}', function ($class='Index', $action='index') {
//    $ctrl = \App::make("\\App\\Http\\Controllers\\admin\\" . $class . "Controller");
//    return \App::call([$ctrl, $action]);
//})->middleware('Lmauthmiddle');

//lm 中间件组
Route::group(['middleware' => 'admin'],function($route){

    Route::get('/admin/', function () {
        $ctrl = \App::make("\\App\\Http\\Controllers\\admin\\MainController");
        return \App::call([$ctrl, 'index']);
    });

    Route::get('/admin/{class}/{action}', function ($class='Index', $action='index') {
        $ctrl = \App::make("\\App\\Http\\Controllers\\admin\\" . $class . "Controller");
        return \App::call([$ctrl, $action]);
    });
    // 这样在访问这个这些路由的时候，就会执行中间件组 admin 所对应的中间件！
});





//自动加载   模块-类-方法
Route::any('/{module}/{class}/{action}', function($module='Index', $class='Index', $action='index') {
    $ctrl = \App::make("\\App\\Http\\Controllers\\" . $module . "\\" . $class . "Controller");
    return \App::call([$ctrl, $action]);
});

////万能视图加载器
Route::get('/admin/html/{view_fold}/{view_file}', function($view_fold,$view_file) {
    return \App::call(
        [App\Http\Controllers\Admin\HtmlController::class, 'index'],
        ['view_fold' => $view_fold,'view_file' => $view_file,]);
});



