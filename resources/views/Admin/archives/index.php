<!DOCTYPE html>
<html>
<!-- 引入头部 -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>文章管理</title>
    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css"/>
    <link rel="stylesheet" href="/js_css/admin/css/admin.css?v=318"/>


    <script type="text/javascript" src="/js_css/admin/layui/layui.js"></script>
    <script type="text/javascript" src="/js_css/admin/js/common.js?v=318"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>


<!-- 主体部分开始 -->
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body layui-row review-filter layui-form">
            <!-- 内容区 -->
            <!-- 功能操作区一 -->

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label w-auto">文章标题：</label>
                        <div class="layui-input-inline mr0">
                            <input type="text" id="title" name="title" placeholder="请输入文章标题" autocomplete="off" class="layui-input">
                        </div>
                    </div>


                    <div class="layui-inline">
                        <select name="typeid" id="typeid" lay-verify="">
                            <?php foreach($result_type as $row_type):?>
                            <option value="<?php echo $row_type['id'];?>"><?php echo $row_type['typename'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="layui-inline">
                        <input class="layui-input" placeholder="开始日期" name="BDATE" id="BDATE">
                    </div>
                    <div class="layui-inline">
                        <input class="layui-input" placeholder="结束日期" name="EDATE" id="EDATE">
                    </div>





                    <div class="layui-inline">
                        <div class="layui-input-inline" style="width: auto;">
                            <button id="search" class="layui-btn" data-type="reload" lay-submit="" lay-filter="search"><i class="layui-icon layui-icon-search"></i></button>

                            <a style="display: none;" href="javascript:" class="layui-btn btnOption  layui-btn-small  btnAdd" id="add" data-param='' lay-event="add">
                                <i class="layui-icon layui-icon-add-1"></i> 增加
                            </a>
                        </div>
                    </div>
                </div>


            <!-- TABLE渲染区 -->

            <table id="listtable" class="layui-table" lay-filter="listfilter"></table>
            <!-- 操作功能区二 -->

        </div>
    </div>
</div>
<!-- 主体部分结束 -->

<script type="text/html" id="imageTpl">
    {{# if(d.image){ }}
    <img src="{{d.image}}" style="width: 100px;">
    {{#} }}
</script>
<script type="text/html" id="activity-coupon-bar">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>


<script>
    $(document).ready(function(){

        $("#add").click(function(){
            layer.open({
                type: 2,
                title:'增加',
                area: ['900px', '700px'], //宽高
                content: '/admin/archives/add',
                success: function(layero, index){

                }
            });
        });

    });

    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#BDATE', //指定元素
            type: 'datetime'
        });

        //执行一个laydate实例
        laydate.render({
            elem: '#EDATE', //指定元素
            type: 'datetime'
        });

    });

    layui.use('table', function(){
        $ = layui.jquery;
        var table = layui.table;
        table.render({
            elem: '#listtable',
            url: '/admin/archives/getList/', //数据接口
            page: true, //开启分页
            limit: 5,
            limits: [5,10,20],
            where: where = {
                title:$('#title').val(),
                typeid:$('#typeid').val(),
                BDATE:$('#BDATE').val(),
                EDATE:$('#EDATE').val(),
            },

            cols: [[ //表头
                {field: 'id', title: 'id', width:90},
                {field: 'title', title: '标题', edit:'text', width:200},
                {field: 'typename', title: '分类', width:100},
                {field: 'image', title: '封面图', width:200, templet: '#imageTpl',event: 'image_big', },
                {title: '操作', toolbar: '#activity-coupon-bar'}
            ]]
        });

        table.on("edit(listfilter)",function(obj){
            var post_data = {id:obj.data.id, title:obj.data.title}
            $.post("/admin/archives/savequick", post_data, function(resObj){
                console.log(resObj);
                if(resObj && resObj.code != 0){
                    layer.open({
                        title: 'error',
                        content: resObj.msg
                    });
                }//
            });
        });

        $("#search").click(function () {
            table.reload('listtable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                url: '/admin/archives/getList/'
                ,where: {
                    title:$('#title').val(),
                    typeid:$('#typeid').val(),
                    BDATE:$('#BDATE').val(),
                    EDATE:$('#EDATE').val(),
                } //设定异步数据接口的额外参数
                //,height: 300
            });
        });


        table.on('tool(listfilter)', function(obj){
            var data = obj.data;
            var layEvent = obj.event;
            console.log(layEvent);
            console.log(obj.data)
            if(layEvent =='image_big')
            {
                layer.open({
                    shadeClose: true,
                    type: 2,
                    title:'图片',
                    area: ['900px', '700px'], //宽高
                    content: obj.data.image,
                    success: function(layero, index){
                    }
                });
                return;
            }
            if(layEvent == 'edit'){
                layer.open({
                    type: 2,
                    title:'增加',
                    area: ['900px', '700px'], //宽高
                    content: '/admin/archives/add?id='+data.id,
                    success: function(layero, index){

                    }
                });
                return;
            }
            if(layEvent == 'del'){
                if(!confirm("确定要删除吗？"))
                {
                    return false;
                }
                var id = data.id;
                var url = '/admin/archives/delete';
                $.post(url,{id:id, enable:0}, function (res) {
                    var resObj = res;
                    if(resObj.code==0)
                    {
                        obj.del();
                    }
                });
                return;
            }

            //short
        });


    });


</script>





</body>
</html>
