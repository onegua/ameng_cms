<?php
//print_r($row);exit;
?>
<!DOCTYPE html>
<html>
<!-- 引入头部 -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>文章管理</title>
    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css"/>
    <link rel="stylesheet" href="/js_css/admin/css/admin.css?v=318"/>


    <script type="text/javascript" src="/js_css/admin/layui/layuist.js"></script>
    <script type="text/javascript" src="/js_css/admin/js/common.js?v=318"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>


<!-- 主体部分开始 -->
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <!-- 内容区 -->
            <!-- 功能操作区一 -->


            <form class="layui-form" id="form" lay-filter="activity-form" action="">
                <input type="hidden" name="id" value="<?php echo isset($row['id']) ? $row['id'] : 0; ?>">

<div style="width:600px">

    <div class="layui-form-item" style="height: 100px;float:right;">
        <div id="image_show">
            <?php if( isset($row['image']) && $row['image']) echo '<img src="'.$row['image'].'" style="width:150px; max-height:100px;">';?>
        </div>
    </div>

<div style="float: left;">

    <div class="layui-form-item">
        <label for="activity_name" class="layui-form-label">
            <span class="x-red">*</span>标题
        </label>
        <div class="layui-input-inline">
            <input type="text" id="title" style="width: 300px;" name="title" required="" lay-verify="required"
                   autocomplete="off" class="layui-input" value="<?php echo isset($row['title']) ? $row['title'] : ''; ?>">
        </div>
    </div>

    <div class="layui-form-item">
        <label for="activity_name" class="layui-form-label">
            <span class="x-red">*</span>分类
        </label>
        <div class="layui-input-inline">
            <input type="text" id="typeid" name="typeid" required="" lay-verify="required"
                   autocomplete="off" class="layui-input layui-hide" value="<?php echo isset($row_type['id']) ? $row_type['id'] : ''; ?>">
            <a class="layui-btn layui-btn-sm layui-btn-danger" id="typeid_select">
                <?php echo isset($row_type['typename']) ? $row_type['typename'] : '选择'; ?>
            </a>




        </div>
    </div>



    <div class="layui-form-item">
        <label for="activity_code" class="layui-form-label">
            <span class="x-red"></span>
        </label>
        <div class="layui-input-inline">
            <input type="text" id="image" name="image" style=""
                   autocomplete="off" class="layui-input" value="<?php echo isset($row['image']) ? $row['image'] : ''; ?>">
        </div>
        <div class="layui-form-mid layui-word-aux"><button type="button" class="layui-btn layui-btn-sm" id="test1">封面图</button></div>

    </div>



</div>
</div>



                <div class="layui-form-item">
                    <label for="activity_name" class="layui-form-label">
                        <span class="x-red">*</span>内容
                    </label>



                    <div class="layui-input-inline" style="width:600px; z-index: 1;">
                        <?php  echo editor($body);?>
                    </div>
                </div>




                <div class="layui-form-item">
                    <label for="activity_code" class="layui-form-label">
                        <span class="x-red"></span>排序号
                    </label>
                    <div class="layui-input-inline" style="width: 100px;">
                        <input type="number" id="short" name="short"
                               autocomplete="off" class="layui-input" value="<?php echo isset($row['short']) ? $row['short'] : ''; ?>">
                    </div>
                </div>



                <div class="layer-footer" style="z-index: 10; position: fixed; text-align: right; margin-left: -10%; bottom: 0; width:100%; height:50px">
                    <button  class="layui-btn" lay-filter="add" lay-submit="">保存</button>
                </div>




            </form>




        </div>
    </div>
</div>
<!-- 主体部分结束 -->



<script>

    layui.use('upload', function(){
        var upload = layui.upload;

        //执行实例
        var uploadInst = upload.render({
            elem: '#test1' //绑定元素
            ,url: '/admin/upload/index' //上传接口
            ,done: function(res){
                //上传完毕回调
                console.log(res);
//                alert(res.msg);
                if(res.code ==0)
                {
                    $("#image").val(res.data.file_path);
                    $("#image_show").html('');
                    $("#image_show").append("<img src='"+res.data.file_path+"' style='width:150px; max-height:100px;'>");
                }
            }
            ,error: function(){
                //请求异常回调
                console.log('error,,,,,,,,,,,');
                alert('error');
            }
        });
    });


    $("#typeid_select").click(function () {
        layer.open({
            type: 2,
            title:'选择分类',
            area: ['900px', '700px'], //宽高
            content: '/admin/arctype/typeid_select?top_id=<?php echo $top_id;?>&selectd_id='+$("#typeid"),
            success: function(layero, index){

            }
        });
    });

    layui.use(['form','layer'], function(){
        $ = layui.jquery;
        var form = layui.form,
            layer = layui.layer;

        selected_typeid();
        form.on('select(typeid)', function(obj){
            selected_typeid();
        });
        function selected_typeid(){
            var storeSearchBox = $('#typeid').parent().find('input');
            storeSearchBox.on('focus',function(e){
                if($('#typeid').val()){
                    storeSearchBox.select();
                }
            });
        }

        //监听提交
        form.on('submit(add)', function(data){

            var params = data.field;
            $.post("/admin/archives/save", params, function(resObj){
                console.log(resObj);
                if(resObj){
                    if(resObj.code == 0){
                        layer.alert(resObj.msg, {icon: 6},function () {
                            var index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.location.reload();
                        });
                    }else{
                        layer.open({
                            title: '保存',
                            content: resObj.msg
                        });
                    }
                    return true;
                }//

                layer.open({
                    title: '保存',
                    content: "数据异常"
                });
            });
            return false;
        });

    });

</script>





</body>
</html>