<?php
//print_r($row);exit;
?>
<!DOCTYPE html>
<html>
<!-- 引入头部 -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>选择分类</title>
    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css"/>
    <link rel="stylesheet" href="/js_css/admin/css/admin.css?v=318"/>


    <script type="text/javascript" src="/js_css/admin/layui/layuist.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>


<!-- 主体部分开始 -->
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <!-- 内容区 -->
            <!-- 功能操作区一 -->


            <div id="typeid_tree" style="">
                <div id="ST-TREE"></div>
            </div>




        </div>
    </div>
</div>
<!-- 主体部分结束 -->



<script>
    //https://layui.itze.cn/doc/modules/tree.html
    layui.tree.render({
        elem: '#ST-TREE'
        , data: <?php echo $layui_tree;?>
        , showCheckbox: true  //是否显示复选框
        , id: 'ST-TREE'
        ,onlyIconControl:true
        ,showCheckbox:false
//        ,search: true
//        , single: true
//        , isJump: true //是否允许点击节点时弹出新窗口跳转
        , click: function (obj) {//文字被点击
            console.log(11);
            <?php $parent_typeid_id = request()->get('typeid_id', 'typeid');?>
            window.parent.$("#typeid").val(obj.data.id);
            window.parent.$("#typeid_select").html(obj.data.title_old);
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            parent.layer.close(index); //再执行关闭
        }
        ,oncheck:function (obj) {//
            console.log('复选框被点击'+obj);
        }
    });


</script>

</body>
</html>