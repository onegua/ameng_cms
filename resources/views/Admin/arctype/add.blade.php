<?php
//print_r($row);exit;
?>
<!DOCTYPE html>
<html>
<!-- 引入头部 -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>文章管理</title>
    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css"/>
    <link rel="stylesheet" href="/js_css/admin/css/admin.css?v=318"/>


    <script type="text/javascript" src="/js_css/admin/layui/layui.js"></script>
    <script type="text/javascript" src="/js_css/admin/js/common.js?v=318"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>


<!-- 主体部分开始 -->
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <!-- 内容区 -->
            <!-- 功能操作区一 -->


            <form class="layui-form" id="form" lay-filter="activity-form" action="">
                <input type="hidden" name="id" value="<?php echo isset($row['id']) ? $row['id'] : 0; ?>">




<div style="width:600px">

    <div class="layui-form-item" style="height: 100px;float:right;">
        <div id="image_show">
            <?php if( isset($row['image']) && $row['image']) echo '<img src="'.$row['image'].'" style="width:150px; max-height:100px;">';?>
        </div>
    </div>

    <div style="float: left;">



                <div class="layui-form-item">
                    <label for="activity_name" class="layui-form-label">
                        <span class="x-red">*</span>上级分类
                    </label>
                    <div class="layui-input-inline">
                        <?php echo $row_parent['typename'];?>

                        <span style="display: none;">
                            <input type="text" id="reid" name="reid" required="" lay-verify="required"
                                   autocomplete="off" class="layui-input" value="<?php echo isset($row_parent['id']) ? $row_parent['id'] : 0; ?>">
                        </span>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="activity_name" class="layui-form-label">
                        <span class="x-red">*</span>分类名称
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="typename" name="typename" required="" lay-verify="required"
                               autocomplete="off" class="layui-input" value="<?php echo isset($row['typename']) ? $row['typename'] : ''; ?>">
                    </div>
                </div>



                <div class="layui-form-item">
                    <label for="activity_code" class="layui-form-label">
                        <span class="x-red"></span>封面图(200*200)
                    </label>
                    <div class="layui-input-block" style="width:300px;">
                        <button type="button" class="layui-btn" id="test1">
                            <i class="layui-icon">&#xe67c;</i>上传图片
                        </button>
                        <input type="text" id="image" name="image"
                               autocomplete="off" class="layui-input" value="<?php echo isset($row['image']) ? $row['image'] : ''; ?>">

                    </div>
                </div>




</div>
</div>






                <div class="layui-form-item">
                    <label for="activity_name" class="layui-form-label">
                        <span class="x-red"></span>内容
                    </label>
                    <div class="layui-input-block">
                        <input type="checkbox" lay-skin="switch" lay-filter="body"  lay-text="显示|隐藏">
                    </div>
                    <div class="layui-input-inline" id="body" style="width:600px; z-index: 1;text-align: left;display: none;">

                        <div>
                        <?php  echo editor(isset($row['body']) ? $row['body'] : '');?>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="activity_code" class="layui-form-label">
                        <span class="x-red"></span>排序号
                    </label>
                    <div class="layui-input-inline">
                        <input type="number" id="sortrank" name="sortrank"
                               autocomplete="off" class="layui-input" value="<?php echo isset($row['sortrank']) ? $row['sortrank'] : 0; ?>">
                    </div>
                </div>




                <div class="layer-footer" style="z-index: 10; position: fixed; text-align: right; margin-left: -10%; bottom: 0; width:100%; height:50px">
                    <button  class="layui-btn" lay-filter="add" lay-submit="">保存</button>
                </div>
            </form>




        </div>
    </div>
</div>
<!-- 主体部分结束 -->



<script>

    layui.use('upload', function(){
        var upload = layui.upload;

        //执行实例
        var uploadInst = upload.render({
            elem: '#test1' //绑定元素
            ,url: '/admin/upload/index' //上传接口
            ,done: function(res){
                //上传完毕回调
                console.log(res);
//                alert(res.msg);
                if(res.code ==0)
                {
                    $("#image").val(res.data.file_path);
                    $("#image_show").html('');
                    $("#image_show").append("<img src='"+res.data.file_path+"' style='width:150px; max-height:100px;'>");
                }
            }
            ,error: function(){
                //请求异常回调
                console.log('error,,,,,,,,,,,');
                alert('error');
            }
        });

    });
</script>

<script>

    layui.use(['form','layer'], function(){
        $ = layui.jquery;
        var form = layui.form,
            layer = layui.layer;
        //监听提交
        form.on('submit(add)', function(data){

            var params = data.field;
            $.post("/admin/arctype/save", params, function(resObj){
                console.log(resObj);
                if(resObj){
                    if(resObj.code == 200){
                        layer.alert(resObj.message, {icon: 6},function () {
                            var index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.location.reload();
                        });
                    }else{

                        layer.open({
                            title: '保存',
                            content: resObj.message
                        });

                    }
                    return true;
                }//

                layer.open({
                    title: '保存',
                    content: "数据异常"
                });
            });
            return false;
        });




        form.on('switch(body)', function(data){
            if(data.elem.checked)
            {
                $("#body").show();
            }else{
                $("#body").hide();
            }
        });






    });

</script>





</body>
</html>