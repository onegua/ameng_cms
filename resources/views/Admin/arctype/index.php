<!DOCTYPE html>
<html>
<!-- 引入头部 -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>分类管理</title>
    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css"/>
    <link rel="stylesheet" href="/js_css/admin/css/admin.css?v=318"/>


    <script type="text/javascript" src="/js_css/admin/layui/layui.js"></script>
    <script type="text/javascript" src="/js_css/admin/js/common.js?v=318"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>


<!-- 主体部分开始 -->
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body layui-row review-filter layui-form" style="width: 700px;">
            <!-- 内容区 -->

            <div style="padding: 20px;">
                <a data_id="0" class="addbtn layui-btn layui-btn-xs">添加顶级分类</a>
            </div>

            <table class="layui-table st-tree" lay-size="sm" lay-skin="nob" id="ST-TREE">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>分类名称</th>
                    <th>操作</th>
                    <th>排序</th>
                    <th>添加时间</th>
                </tr>
                </thead>
                <tbody>

<?php
$color = ['black','black','red','#0000cc','green','#0000cc'];
foreach($result_type as $row_type):
    $son_padding    = $row_type["step"] * 20;
    $son_size       = 20 - $row_type["step"] * 2;
    $son_heng       = str_pad("",$row_type["step"],"-");
    $son_color      = isset($color[$row_type["step"]]) ? $color[$row_type["step"]] : "black";
    ?>
                <tr id="tr_<?php echo $row_type["id"];?>" data-level="<?php echo $row_type["level"];?>" data-indent="<?php echo $row_type["reid"];?>" data-open="1">


                    <td><?php echo $row_type["id"];?></td>
                    <td>

                        <span style="padding-left: <?php echo $son_padding;?>px;
                                color:<?php echo $son_color;?>;
                                font-size:<?php echo $son_size;?>px;">
                            |
                            <?php echo $son_heng;?>
                            <?php echo $row_type["typename"];?>
                            <?php //echo $row_type["level"];?>
                        </span>
                    </td>
                    <td>
                        <a data_id="<?php echo $row_type["id"];?>" class="addbtn layui-btn layui-btn-xs">添加</a>
                        <a data_id="<?php echo $row_type["id"];?>" class="editbtn layui-btn layui-btn-xs layui-btn-normal" >更新</a>
                        <a href="" target="_blank" class="layui-btn layui-btn-xs st-btn-bg-succ">列表</a>
                        <a data_id="<?php echo $row_type["id"];?>" class="deletebtn layui-btn layui-btn-danger layui-btn-xs">删除</a>
                    </td>

                    <td><?php echo $row_type["sortrank"];?></td>
                    <td><?php echo $row_type["created_time"];?></td>
                </tr>

<?php endforeach;?>



                </tbody>
            </table>






        </div>
    </div>
</div>
<!-- 主体部分结束 -->

<script src="/js_css/admin/js/jquery-3.5.1.min.js"></script>
<script src="/js_css/admin/js/util.js"></script>

<script>
    // 这里引用strongshop的JS功能
//    !function () {
//        Util.treeTable("#ST-TREE");
//    }();

    layui.use(['form','layer'], function(){

        $(".addbtn").click(function(){
            layer.open({
                type: 2,
                title:'增加',
                area: ['900px', '700px'], //宽高
                content: '/admin/arctype/add?reid='+$(this).attr("data_id"),
                success: function(layero, index){

                }
            });
        });

        $(".editbtn").click(function(){
            layer.open({
                type: 2,
                title:'增加',
                area: ['900px', '700px'], //宽高
                content: '/admin/arctype/add?id='+$(this).attr("data_id"),
                success: function(layero, index){

                }
            });
        });


        $(".deletebtn").click(function () {
            if(!confirm("该分类及所有子类和关联文章将被删除【不可恢复】"))
            {
                return true;
            }
            var id      = $(this).attr("data_id");
            var params = {id:id};
            $.post("/admin/arctype/delete",params, function (res) {
                var resObj = res;
                if(!resObj)
                {
                    alert("服务端异常");
                    return false;
                }
                alert(resObj.message);
                if(resObj.code==200)
                {
                    $("#tr_"+id).remove();
                    return true;
                }
                return false;
            });

        });
    });

    $(function () {

    });
</script>





</body>
</html>
