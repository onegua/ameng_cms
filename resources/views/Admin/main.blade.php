
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>CMS</title>
    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css"/>
    <link rel="stylesheet" href="/js_css/admin/css/admin.css?v=318"/>
    <link rel="stylesheet" href="/js_css/admin/css/theme-all.css?v=318"/>
<?php
//    Meta::addJs('admin_js', '/resources/assets/admin_js/admin_app.js');//resources中获取文件

    ?>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <!-- 头部 -->
    <div class="layui-header">
        <div class="layui-logo">
            <img src="/js_css/admin/images/logo.jpg"/>
            <cite>&nbsp;&nbsp;CMS&emsp;</cite>
        </div>
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item" lay-unselect>
                <a ew-event="flexible" title="侧边伸缩"><i class="layui-icon layui-icon-shrink-right"></i></a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a ew-event="refresh" title="刷新"><i class="layui-icon layui-icon-refresh-3"></i></a>
            </li>
            <li class="layui-nav-item layui-hide-xs layui-this " lay-unselect><a
                        nav-bind="xt1">系统管理</a></li>
            <li class="layui-nav-item layui-hide-xs " lay-unselect><a
                        nav-bind="xt2">运营管理</a></li>
            <li class="layui-nav-item layui-hide-xs " lay-unselect><a
                        nav-bind="xt3">内容管理</a></li>
            <li class="layui-nav-item layui-hide-xs " lay-unselect><a
                        nav-bind="xt4">会员管理</a></li>
            <!-- 小屏幕下变为下拉形式 -->
            <li class="layui-nav-item layui-hide-sm layui-show-xs-inline-block" lay-unselect>
                <a>更多</a>
                <dl class="layui-nav-child">
                    <dd lay-unselect><a nav-bind="xt1">系统一</a></dd>
                    <dd lay-unselect><a nav-bind="xt2">系统二</a></dd>
                    <dd lay-unselect><a nav-bind="xt3">系统二</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item" lay-unselect>
                <a ew-event="message" title="消息">
                    <i class="layui-icon layui-icon-notice"></i>
                    <span class="layui-badge-dot"></span>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a ew-event="note" title="便签"><i class="layui-icon layui-icon-note"></i></a>
            </li>
            <li class="layui-nav-item layui-hide-xs" lay-unselect>
                <a ew-event="fullScreen" title="全屏"><i class="layui-icon layui-icon-screen-full"></i></a>
            </li>
            <li class="layui-nav-item layui-hide-xs" lay-unselect>
                <a ew-event="lockScreen" title="锁屏"><i class="layui-icon layui-icon-password"></i></a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a>
                    <img src="/js_css/admin/images/logo.jpg" class="layui-nav-img">
                    <cite>管理员</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd lay-unselect><a ew-href="/index/userinfo">个人中心</a></dd>
                    <dd lay-unselect><a ew-event="psw">修改密码</a></dd>
                    <hr>
                    <dd lay-unselect><a ew-event="logout" data-url="/admin/login/logout">退出</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a ew-event="theme" title="主题"><i class="layui-icon layui-icon-more-vertical"></i></a>
            </li>
        </ul>
    </div>

    <!-- 侧边栏 -->
    <div class="layui-side">
        <div class="layui-side-scroll">
            <ul class="layui-nav layui-nav-tree " nav-id="xt1" lay-filter="admin-side-nav" lay-shrink="_all" style="margin: 15px 0;">
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>权限管理</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/admin/index">用户管理</a></dd>
                        <dd><a lay-href="/role/index">角色管理</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>关于我们</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/admin/archives/indexabout">关于我们列表</a></dd>
                        <dd><a lay-href="/admin/archives/addabout">增加关于我们</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>资讯</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/admin/archives/indexnews">资讯列表</a></dd>
                        <dd><a lay-href="/admin/archives/addnews">增加资讯</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>产品</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/admin/archives/indexproduct">产品列表</a></dd>
                        <dd><a lay-href="/admin/archives/addproduct">增加产品</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a lay-href="/admin/arctype/index"><i class="layui-icon layui-icon-component"></i>&emsp;<cite>分类管理</cite></a>
                </li>

            </ul>
            <ul class="layui-nav layui-nav-tree layui-hide " nav-id="xt2" lay-filter="admin-side-nav" lay-shrink="_all" style="margin: 15px 0;">
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>运营管理1</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/adsort/index">111</a></dd>
                        <dd><a lay-href="/ad/index">112</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>运营管理2</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/layoutdesc/index">22</a></dd>
                        <dd><a lay-href="/layout/index">23</a></dd>
                    </dl>
                </li>

            </ul>
            <ul class="layui-nav layui-nav-tree layui-hide " nav-id="xt3" lay-filter="admin-side-nav" lay-shrink="_all" style="margin: 15px 0;">
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>内容1</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/item/index">11</a></dd>
                        <dd><a lay-href="/itemcate/index">12</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>22</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/article/index">23</a></dd>
                    </dl>
                </li>
            </ul>
            <ul class="layui-nav layui-nav-tree layui-hide " nav-id="xt4" lay-filter="admin-side-nav" lay-shrink="_all" style="margin: 15px 0;">
                <li class="layui-nav-item">
                    <a><i class="layui-icon layui-icon-component"></i>&emsp;<cite>会员管理</cite></a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/userlevel/index">会员等级</a></dd>
                        <dd><a lay-href="/user/index">会员管理</a></dd>
                    </dl>
                </li>
            </ul>

        </div>
    </div>

    <!-- 主体部分 -->
    <div class="layui-body"></div>
    <!-- 底部 -->
    <div class="layui-footer layui-text">
        copyright © 2017~2021   all rights reserved.

        <span class="pull-right">版本号：v2.0.3</span>
    </div>
</div>



<!-- js部分 -->
<script type="text/javascript" src="/js_css/admin/layui/layui.js"></script>
<script type="text/javascript" src="/js_css/admin/js/common.js?v=318"></script>
<script>
    layui.use(['index'], function () {
        var $ = layui.jquery;
        var index = layui.index;

        // 默认加载主页
        index.loadHome({
            menuPath: '/admin/html/index',
            menuName: '<i class="layui-icon layui-icon-home"></i>'
        });

    });
</script>
</body>
</html>
