<?php
//print_r($row);exit;
?>
        <!DOCTYPE html>
<html>
<!-- 引入头部 -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>登录</title>
    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css"/>
    <link rel="stylesheet" href="/js_css/admin/css/admin.css?v=318"/>


    <script type="text/javascript" src="/js_css/admin/layui/layui.js"></script>
    <script type="text/javascript" src="/js_css/admin/js/common.js?v=318"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
<div class="layui-container st-login-box">


    <div class="st-login">
        <fieldset class="layui-elem-field layui-field-title">
            <legend>CMS 后台管理</legend>
        </fieldset>


            <form class="layui-form" id="form" lay-filter="activity-form" action="" style="width:300px;">

            <div class="layui-form-item">
                <label class="layui-form-label">登录用户</label>
                <div class="layui-input-block">
                    <input type="text" name="username" id="username" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">登录密码</label>
                <div class="layui-input-block">
                    <input type="password" name="password" id="password" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">验证码</label>
                <div class="layui-input-block">
                    <input type="text" name="captcha" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-block">
                    <img class="captcha" src="/admin/login/captcha" onclick="this.src='/admin/login/captcha?'+Math.random()" title="点击刷新" />
                </div>
            </div>


            <div class="layui-form-item">
                <label for="L_repass" class="layui-form-label"></label>
                <button  class="layui-btn" lay-filter="add" lay-submit="">登录</button>
            </div>
        </form>
    </div>
</div>

<script>

    layui.use(['form','layer'], function(){
        $ = layui.jquery;
        var form = layui.form,
            layer = layui.layer;
        //监听提交
        form.on('submit(add)', function(data){

            var params = data.field;
            $.post("/admin/login/index", params, function(resObj){
                console.log(resObj);
                if(resObj.code){
                    layer.open({
                        title: '登录',
                        content: resObj.message
                    });
                    return false;
                }//

                window.location.href=resObj.url;
            });
            return false;
        });

    });

</script>


</body>
</html>