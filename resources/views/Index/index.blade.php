
<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Layui 开发使用文档 - 入门指南</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="/js_css/admin/layui/css/layui.css " media="all">
    <link rel="stylesheet" href="/js_css/global.css" media="all">
</head>

<body>


<div class="layui-header header header-doc" autumn="">
    <div class="layui-container">
        <a class="logo" href="/">
            <span style="color: #fff;">CMS - 轻松建站从此开始！</span>
        </a>
        <div class="layui-form layui-hide-xs component" lay-filter="LAY-site-header-component"></div>
        <div class="layui-hide-xs site-notice"></div>

        <ul class="layui-nav" id="LAY_NAV_TOP">
            <li class="layui-nav-item layui-this">
                <a href="">文档</a>
            </li>
            <li class="layui-nav-item ">
                <a href="../demo/index.html">示例</a>
            </li>
            <li class="layui-nav-item ">
                <a href="https://layui.itze.cn/doc/modules/layer.html" target="_blank">弹出层（layer）</a>
            </li>

            <li class="layui-nav-item layui-hide-xs">
                <a href="https://jq.qq.com/?_wv=1027&k=R7OGAdYx" target="_blank" rel="nofollow">QQ群：23031273</a>
            </li>
            <div class="layui-footer footer footer-doc"></div>
        </ul>
    </div>
</div>

<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<ul class="site-dir">
    <li><a href="#get"><cite>获得 layui</cite></a></li>
    <li><a href="#quickstart"><cite>快速上手</cite></a></li>
    <li><a href="#classical"><cite>何为「经典」</cite></a></li>
    <li><a href="#modules"><cite>建立模块入口</cite></a></li>
    <li><a href="#extmodules"><cite>管理扩展模块</cite></a></li>
</ul>
<div class="layui-container layui-row">

    <div class="layui-col-md3">
        <div class="layui-panel site-menu">
            <ul class="layui-menu layui-menu-lg">
                <li class="layui-menu-item-group" lay-options="{type: 'group', isAllowSpread: true}">
                    <div class="layui-menu-body-title">
                        基础说明
                    </div>
                    <hr>
                    <ul>





                        <li class="">
                            <div class="layui-menu-body-title">
                                <a href="base/faq.html">
                                    <span>常见问题 </span>
                                    <span class="layui-font-12 layui-font-gray">FAQ</span>
                                </a>
                            </div>
                        </li>
                        <li class="">
                            <div class="layui-menu-body-title">
                                <a href="base/changelog.html">
                                    <span>更新日志 </span>
                                    <span class="layui-font-12 layui-font-gray">changelog</span>

                                    <span class="layui-badge-dot"></span>

                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


            </ul>
        </div>
        <div class="layui-hide-v"> - </div>
    </div>


    <div class="layui-col-md9 site-content">
        <fieldset class="layui-elem-field layui-field-title site-title">
            <legend><a name="compatibility">兼容性和面向场景</a></legend>
        </fieldset>

        <blockquote class="layui-elem-quote" style="text-align: center;">
            layui 兼容人类正在使用的全部浏览器（IE6/7除外），可作为 Web 界面速成开发方案。
        </blockquote>



    </div>
</div>


</body>

</html>
