<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Elasticsearch\ClientBuilder;

class EScreateIndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'es:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $host = config('scout.elasticsearch.hosts');
        $index = config('scout.elasticsearch.index');
        $client = ClientBuilder::create()->setHosts($host)->build();

        if ($client->indices()->exists(['index' => $index])) {
            $this->warn("Index {$index} exists, deleting...");
            $client->indices()->delete(['index' => $index]);
        }

        $this->info("Creating index: {$index}");

        return $client->indices()->create([
            'index' => $index,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0
                ],
                'mappings' => [
                    '_source' => [
                        'enabled' => true
                    ],
                    'properties' => [
                        'mapping' => [ // 字段的处理方式
                            'type' => 'keyword', // 字段类型限定为 string
                            'fields' => [
                                'raw' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256, // 字段是索引时忽略长度超过定义值的字段。
                                ]
                            ],
                        ],
                    ],
                ]
            ]
        ]);

        return Command::SUCCESS;
    }
}
