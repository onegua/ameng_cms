<?php

namespace App\Http\Classes;

use Illuminate\Http\Request;

class Lmauth
{
    public $AdminUser;
    public $request;

    function __construct()
    {
    }

    function islogin()
    {
        return $this->getAdminUser() ? true : false;
    }

    function login($AdminUser)
    {
        $this->AdminUser   = $AdminUser;
        request()->session()->put('AdminUser', $AdminUser);
    }

    function check()
    {
        if(!$this->getAdminUser())
        {
            return false;
        }
        return true;
    }

    function getAdminUser()
    {
        if(!$this->AdminUser)
        {
            $this->AdminUser = request()->session()->get('AdminUser');
        }
        return $this->AdminUser;
    }

    function logout()
    {
//        session(['AdminUser'=>'']);
        request()->session()->forget('AdminUser');
    }

}
