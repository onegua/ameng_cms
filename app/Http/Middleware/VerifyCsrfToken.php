<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'admin/*'   //所有admin的路由不作CSRF验证 （默认在中间件中加载了csrf-VerifyCsrfToken, App\Http\Kernel）
    ];
}
