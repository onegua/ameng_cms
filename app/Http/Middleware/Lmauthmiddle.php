<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Classes\Lmauth;
use App\Models\AdminRoleModel;


class Lmauthmiddle
{
    public $Lmauth;
    function __construct()
    {
        $this->Lmauth    = new Lmauth();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array(myuri(), $this->getIgnoreUrl()))
        {
            return $next($request);
        }
        if(!$this->Lmauth->islogin())
        {
            request()->session()->flash('fullurl', $request->fullUrl());
            return redirect("/admin/login/index");
        }

        if (!$this->checkPermission($request))
        {
            return abort(403, '暂无权限');
        }

        return $next($request);
    }

    /**
     * 访问权限验证
     * @param $request
     */
    function checkPermission($request)
    {
        $user       = $this->Lmauth->getAdminUser();
        if ($user['id'] == 1)
        {
            //超级管理员角色
            return true;
        }

        //获取角色对应的route_url
        $roleModel = AdminRoleModel::find($user['admin_role_id']);
        if ($roleModel)
        {
            $menu_route_url_ary =@json_decode($roleModel->menu_route_url, true);
//            $menu_route_url_ary = array_merge($menu_route_url_ary, $this->getIgnoreUrl());
            //如果当前url是 不在 角色组中，则返回false
            if (in_array(myuri(), $menu_route_url_ary))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 以下这几个URI不用验证登录状态
     * 'ignore_auth_check_url' => ['strongadmin/login', 'strongadmin/logout', 'strongadmin/captcha'],
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getIgnoreUrl()
    {
        return ['admin/login/index','admin/login/captcha',
            'admin/login/logout',
            'admin/html/index',
        ];
    }


}
