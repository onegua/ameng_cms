<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Elasticsearch\ClientBuilder;
use App\Models\ArchivesModel;


class MainController extends Controller
{
    public $client = null;
    public $search = '';

    public function __construct() {
        $this->client = ClientBuilder::create()->build();
    }

    /**
     * https://www.cnblogs.com/hualess/p/11540477.html      安装服务
     * https://blog.csdn.net/weixin_34650292/article/details/121845614  安装扩展
     * [示例]：http://www.amengcms.com/admin/main/elasticsearch?keywords=介绍
     * elasticsearch 全文搜索索引，分词搜索
     * 1 下载安装（windows）ElasticSearch下载地址：https://www.elastic.co/downloads/elasticsearch
     *      进入bin目录下,双击执行elasticsearch.bat
     *      http://localhost:9200 看到json串输出说明启动成功,打开浏览器测试一下,如下图
     * 2 laravel安装扩展包
     *  composer require  elasticsearch/elasticsearch //elasticsearch核心包
        composer require tamayo/laravel-scout-elastic
        composer require laravel/scout "^8.x-dev" --dev
        php artisan vendor:publish
       最后 我们选择 9 就行啦！，之后我们要到config目录下的app.php文件中去添加上我们的elasticsearch服务
     * Laravel\Scout\ScoutServiceProvider::class
     * 然后，我们找到config目录下的scout.php文件，修改驱动为elasticsearch
     * 'driver' => env('SCOUT_DRIVER', 'elasticsearch'),
     * 并在下方添加驱动：
     *     'elasticsearch' => [
                'hosts' => [
                    env('ELASTICSEARCH_HOSTS','http://127.0.0.1:9200')
                ],
                'index' => env('ELASTICSEARCH_INDEX','laravel_es_test'),
            ]
     * 在.env文件中配置上你的elasticsearch服务器地址及索引名称
            ELASTICSEARCH_HOSTS= 你的服务器地址:9200
            ELASTICSEARCH_INDEX= 你创建的索引名称
    接着我们在elasticsearch中创建好索引，当索引创建完毕后，我们就可以通过laravel的command进行索引的创建和初始化。
    php artisan make:command EScreateIndexCommand
    然后在Console/Kernel.php中挂载上生成的类
    protected $commands = [
        \App\Console\Commands\EScreateIndexCommand::class
    ];
    如果laravel版本在8.0以上可以跳过这个步骤。来到我们创建的EScreateIndexCommand.php中，写入以下代码：见项目代码。
     * 随后在控制台运行这个文件指令(以上这些操作就是为了下面这行，可执行命令。)
     * php artisan es:create
     * 这样我们索引也就弄好了！ 接着，我们需要创建一个model对象来将我们数据填入elasticsearch索引中去。
     * php artian make:model ArchivesModel
     * 请根据你自己的数据表情况而自行定义，我的仅供参考 紧接着运行指令，导入我们数据
     * php artisan scout:import "App\Models\ArchivesModel"
     * 最后，我们创建一个控制器类，来测试我们的elasticsearch是否配置成功    创建控制器的步骤我就不做赘述，验证方法如下:
     * 【注】数据库表数据变化后需要重新import，或者刷新索引。（这个类似于在缓存中查找，所以需要刷新下）
     * @param Request $request
     */
    public function elasticsearch(Request $request)
    {
        $keywords   = $request->keywords;
        $res        = ArchivesModel::search($keywords)->get();
        print_r(json_decode(json_encode($res), true));exit;
        print_r($res);exit;

        $a = new ArchivesModel();
        $a = ArchivesModel::where(['id'=>20])->get()->first();
        print_r(json_decode(json_encode($a), true));exit;
        print_r($res);exit;
    }

    //创建
    public function index33(){
        $name = "Q1";
        $res = ArchivesModel::search($name)->get();
        $a = new ArchivesModel();
        print_r($a->getAttributes());exit;
       print_r($res);exit;


        $params = [
            'index' => '',
            'type' => '_doc',
            'id' => 'my_id',
            'body' => ['testField' => 'abc']
        ];
print_r($this->client->index($params));exit;
        $response = $this->client->index($params);
        print_r($response);
    }

    public function index(Request $request)
    {
//        echo 'MainController'; exit;
        return view('admin/main');
    }

}


