<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\ArctypeModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Admin\BaseController;

class ArctypeController extends BaseController
{
    public $type_id     = 0;

    //view
    public function index()
    {
        $data           = ['result_type'=>get_son_typeids_rows( 0)];
        return view('admin/arctype/index',$data);
    }

    //view
    public function add(Request $request)
    {
        $row_parent     = DB::table('arctype')->where('id',$request->reid)->get()->first();
        $row            = DB::table('arctype')->where('id',$request->id)->get()->first();//分类
        $row            = $row ? json_decode(json_encode($row),true) : [];
        $row_parent     = $row_parent ? json_decode(json_encode($row_parent),true) : [];
        if(!$row_parent) $row_parent = ['id'=>0,'typename'=>'顶级分类'];
        $data           = [
            'row'=>$row,
            'row_parent'=>$row_parent,
            'id'=>$request->id,
        ];
        return view('admin/arctype/add',$data);
    }


    /**
     * 增加或修改
     * @param Request $request
     * @param ArctypeModel $category
     * @return array
     */
    public function save(Request $request, ArctypeModel $category)
    {
        $rules          = array_merge_recursive($category->rules(), [
//            'typename' => [Rule::unique('arctype')->where("reid", $request->reid ?: " ")->ignore($request->id)],//Rule::unique 验证唯一性
        ]);

        //感觉用orm写法，读起来吃力。简单的SQL用用还好，复杂的SQL还是使用原生SQL可读性强些，so:
        $row_has        = DB::table('arctype')->where('typename',$request->typename)->get()->first();
        if($row_has && ($row_has->reid == $request->reid) && ($row_has->id !=$request->id))
        {
            return ['code' => 3001, 'message' => '分类下存在相同的子类名称'];
        }

        $messages       = $category->messages();
        $customAttributes = $category->customAttributes();
        $validator      = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if ($validator->fails())
        {
            return ['code' => 3001, 'message' => $validator->errors()->first(), 'data' => $validator->errors()];
        }

        $category->fill($request->all());

        //update
        if($request->id)
        {
//            $category = $category::find($request->id);//大家注意了。这里很不爽。一会$model,一会$category
            $model = $category::where(['id'=>$request->id]);//大家注意了。这里很不爽。一会$model,一会$category
            if (!$model->update($category->getAttributes()))
            {
                return ['code' => 5001, 'message' => '保存失败'];
            }
            return [ 'code' => 200,'message' => '更新成功','data' => $category->id,];
        }

        //add
        if (!$category->save())
        {
            return ['code' => 5001, 'message' => '保存失败'];
        }
        return [ 'code' => 200,'message' => '保存成功','data' => $category->id,];
    }

    /**
     * 删除
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required',],
        ]);
        if ($validator->fails())
        {
            return ['code' => 3001, 'message' => $validator->errors()->first(), 'data' => $validator->errors()];
        }
        $category       = new ArctypeModel;
        if (!$category->delete($request->id))
        {
            return ['code' => 5001, 'message' => '删除失败'];
        }
        return [
            'code' => 200,
            'message' => '删除成功',
            'log' => sprintf('[%s][%s]『id:%s』', '删除成功', $category->tableComments, $request->id)
        ];
    }

    /**
     * 选择分类，供其他页面引用
     */
    function typeid_select()
    {
        $top_id                 = request()->get('top_id',0);
        $selectd_id             = request()->get('selectd_id', 0);
        $data['layui_tree']    = json_encode([layui_tree($top_id, $selectd_id)]);
        return view('admin/arctype/typeid_select',$data);
    }

    /**
     * 用模型ORM来删除。总觉得怪怪的：：
     * --------------------$category        = new ArctypeModel;
     * --------------------$model          = $category::whereIn('id', $ids);
     * --------------------$model->delete()
     *
     * @param Request $request
     * @return array
     */
    function deleteYY(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required',],
        ]);
        if ($validator->fails())
        {
            return ['code' => 3001, 'message' => $validator->errors()->first(), 'data' => $validator->errors()];
        }
        $ids            = is_array($request->id) ? : [$request->id];
        $category       = new ArctypeModel;
        $model          = $category::whereIn('id', $ids);//这里使用$model，可能是因为whereIn返回的是静态方法吧
        if (!$model->delete())
        {
            return ['code' => 5001, 'message' => '删除失败'];
        }
        return [
            'code' => 200,
            'message' => '删除成功',
            'log' => sprintf('[%s][%s]『id:%s』', '删除成功', $category->tableComments, json_encode($ids))
        ];
    }

}


