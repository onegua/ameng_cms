<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * 万能视图加载器
 * Class HtmlController
 * @package App\Http\Controllers\Admin
 */
class HtmlController extends Controller
{

    public function index(Request $request)
    {
        $uri_path       =  $request->path();
        $view           = str_replace("admin/html", "", $uri_path);
        return view('admin/'.$view);
    }
}


