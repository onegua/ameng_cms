<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    function index()
    {
        return $this->image();
    }
    //上传图片
    public function image()
    {
        $file           = request()->file('file');
        $folder_name    = "uploads/".date('ymd')."/";
        $upload_path    = public_path() . "/" . $folder_name;
        $extension      = strtolower($file->getClientOriginalExtension()) ?: 'png';
        $filename       = time().rand(100,999).'.'. $extension;
        //将图片移动到我们目标存储路径中
        $info           = $file->move($upload_path , $filename);

        $file_path  = '/'.$folder_name.$filename;
        $data       = [
            'filename' =>$filename,
            'file_path' =>$file_path,
            'alt'=>$file->getClientOriginalName(),
            'web_url' =>asset($file_path),
        ];
        return ['code' => 0,'msg' => '成功','data'=>$data];
    }

    function wangeditor()
    {
        $upload_data    = $this->image()['data'];
        $upload_data['href'] = $upload_data['file_path'];
        $upload_data['url'] = $upload_data['file_path'];
        $data[] = $upload_data;
        return ['code' => 0,'errno' => 0, 'message' => 'success', 'data' => $data];
    }

}


