<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ArchivesController extends Controller
{
    public $top_id     = 0;

    function indexabout()
    {
        $this->top_id  = 1;
        return $this->index();
    }

    function addabout()
    {
        $this->top_id  = 1;
        return $this->add();
    }

    function indexnews()
    {
        $this->top_id  = 2;
        return $this->index();
    }

    function addnews()
    {
        $this->top_id  = 2;
        return $this->add();
    }

    function indexproduct()
    {
        $this->top_id  = 3;
        return $this->index();
    }

    function addproduct()
    {
        $this->top_id  = 3;
        return $this->add();
    }

    function result_type()
    {
        $typeids   = get_son_typeids( $this->top_id);
        $result_type    = DB::table('arctype')->whereIn('id',$typeids)->get()
            ->map(function ($value){
                return (array)$value;
            })->toArray();//分类
        return $result_type;
    }

    public function index()
    {
        if(request()->get('typeid')) $this->top_id = (int)request()->get('typeid');
        $data           = ['result_type'=>$this->result_type(),'typeid'=>$this->top_id];
        return view('admin/archives/index',$data);
    }

    public function add()
    {
        $data           = [];
        if($id  = request()->get('id'))
        {
            $where              = ['id'=>$id];
            $row                = DB::table('archives')->where($where)->first();
            if($row)
            {
                $row            = json_decode(json_encode($row), true);
                $row['body']    = DB::table('addonarticle')->where(['aid'=>$id])->pluck('body')->first();
                $data['row']    = $row;
                $this->top_id   = get_top_parentid($row['typeid'],[],1)['id'];

                $row_type                = DB::table('arctype')->where(['id'=>$row['typeid']])->first();
                $data['row_type']       = json_decode(json_encode($row_type), true);
            }//
        }
        $top_type                = DB::table('arctype')->where(['id'=>$this->top_id])->first();
        $top_type                = [json_decode(json_encode($top_type), true)];
        $result_type             = get_son_typeids_rows($this->top_id);
        $data['result_type']    = array_merge($top_type, $result_type);

//        return $data['layui_tree'];
        $data['top_id']         = $this->top_id;
        $data['body']           = isset($row) && $row ? $row['body'] : '';
        return view('admin/archives/add',$data);
    }

    public function getList(Request $request)
    {
        $page       = request()->get('page', 1);
        $limit      = request()->get('limit', 5);
        $offset     = ($page-1) * $limit;

        $title       = request()->get("title");
        $typeid      = request()->get("typeid");
        $BDATE       = request()->get("BDATE");
        $EDATE       = request()->get("EDATE");
//        $typeid = 3;
        $where       = [];
        if($title)
        {
            $where[] = ['title', 'like', '%'.$title.'%'];
        }
        if($BDATE && $EDATE)
        {
            $where[] = ['created_time', '>=', $BDATE];
            $where[] = ['created_time', '<=', $EDATE];
        }

        $query      = DB::table('archives')->where($where);
        if($typeid)
        {
            $query  = $query->whereIn('typeid', get_son_typeids( $typeid));
        }

//        DB::connection()->enableQueryLog();//getlastsql getlastquery 打印出查询最后执行的SQL
        $count = $query->count();
//        print_r(DB::getQueryLog());exit; //getlastsql 打印出最后执行SQL一条

        $result = $query
            ->orderby('id', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get()
            ->map(function ($value){
                return (array)$value;
            })->toArray();
//print_r($result);exit;
        $result_type    = $this->getarctype();
        foreach ($result as $k=>$row)
        {
            $row['typename']   = isset($result_type[$row['typeid']]) ? $result_type[$row['typeid']] : '';
            $result[$k]     = $row;
        }

        $output = [
            'code' => 0,
            'msg' => '',
            'count' => $count,
            'data' => $result
        ];
        return $output;
    }

    function getarctype()
    {
        $result_type    = DB::table('arctype')->get();
        $result_type_new = [];
        foreach ($result_type as $row_type)
        {
            $result_type_new[$row_type->id] = $row_type->typename;
        }
        return $result_type_new;
    }

    /**
     * 快速修改编辑
     */
    function savequick()
    {
        $id             = request()->post('id');
        $title           = request()->post('title');
        if(!$title)
        {
            return response()->json(['code' => 1,'msg' => '请填写标题']);
        }
        $insertData     = ['title'=>$title];
        DB::table('archives')->where(['id'=>$id])->update($insertData);
        $output = [
            'code' => 0,
            'msg' => '成功',
            'data' => $id
        ];
        return $output;
    }
    function save()
    {
        $id             = request()->post('id');
        $title           = request()->post('title');
        $typeid          = (int)request()->post('typeid');
        $short          = (int)request()->post('short');
        $body           = request()->post('body');
        $image          = request()->post('image');
        $created_time   = date("Y-m-d H:i:s");

        if(!$title)
        {
            return response()->json(['code' => 1,'msg' => '请填写标题']);
        }
        if(!$typeid)
        {
            return response()->json(['code' => 1,'msg' => '请选择分类']);
        }

        $insertData     = [
            'title'      => $title,
            'short'     => $short,
            'typeid'     => $typeid,
            'image'     => $image,
            'created_time'=> $created_time,
        ];
//        print_r($insertData);exit;
        $row        = DB::table('archives')->where(['id'=>$id])->first();
        if($row) //update
        {
            DB::table('archives')->where(['id'=>$id])->update($insertData);
            DB::table('addonarticle')->where(['aid'=>$id])->delete();
        }else{
            $id    = DB::table('archives')->insertGetId($insertData);
        }
        DB::table('addonarticle')->insert(['body'=>$body,'aid'=>$id]);

        $output = [
            'code' => 0,
            'msg' => '成功',
            'data' => $id
        ];
        return $output;
    }

    function delete()
    {
        $id             = request()->post('id');

        //事务 https://learnku.com/docs/laravel/8.5/database/10403#database-transactions
//        DB::transaction(function ($id) {
//            echo $id;exit;
//        }, 5);

        DB::beginTransaction();
        try {
            DB::table('archives')->where(['id'=>$id])->delete();
            DB::table('addonarticle')->where(['aid'=>$id])->delete();
            DB::commit();//必须放在最后一行。commit后不能回滚，在一个事务中，rollback和commit都代表结束一个事务。要么回滚要么提交。他们是在一个等级上的命令
        }catch(\Exception $e){//注，要么代码在最开头使用 use \Exception,要么使用catch (\Exception $e)
//            var_dump(666);exit;
            DB::rollBack();
            return response()->json(['code' => 1,'msg' => '删除失败:'.$e->getMessage()]);
        }

        $output = [
        'code' => 0,
        'msg' => '成功',
        'data' => $id
        ];
        return response()->json($output);
    }

    public function showPost($slug){
        $post=Post::where('slug',$slug)->firstOrFail();
        return view('blog.post',['post'=>$post]);
    }


}


