<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Models\AdminUserModel;
use App\Models\ArctypeModel;
use App\Http\Classes\Lmauth;

class LoginController extends Controller
{

    public $maxAttempts; //允许尝试登录最大次数
    public $decayMinutes; //登录错误超过 maxAttempts 次, 禁止登录 decayMinutes 分钟
    public $Lmauth;

    public function __construct()
    {
        $this->maxAttempts = 3;
        $this->decayMinutes =5;
        $this->Lmauth    = new Lmauth();
    }

    public function index(Request $request)
    {
        if (!$request->expectsJson())
        {
            if($this->Lmauth->islogin())
            {
                return redirect("/admin/main/index");
            }
            return view('admin/login');
        }
        $rules = [
            'username' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
        if (!\App::environment(['local', 'testing']))
        {
            $rules = array_merge($rules, ['captcha' => 'required|captcha']);
        }
        $messages = [
            'captcha.*' => ':attribute 不正确。',
            'username.*' =>'用户名必填',
            'password.*' =>'密码必填',
        ];
        $customAttributes = [
            'captcha' => '验证码',
        ];
        $validator = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if ($validator->fails())
        {
            return ['code' => 3001, 'message' => $validator->errors()->first(), 'data' => $validator->errors()];
        }

        if (\Cache::get('login_error_nums')>$this->maxAttempts)
        {
            return ['code' => 4001, 'message' => "登录失败次数过多,请{$this->decayMinutes}分钟后重试", 'log' => "登录失败次数过多：{$request->username}"];
        }
        $category       = new AdminUserModel;
        $admin_user = AdminUserModel::where('user_name', $request->username)->first();
        if (!$admin_user)
        {
            return ['code' => 4002, 'message' => '登录失败', 'log' => "登录失败（账号不存在）：{$request->username}"];
        }
        if ($admin_user->status !== 1)
        {
            return ['code' => 4003, 'message' => '登录失败', 'log' => "登录失败（账号禁用）：{$request->username}"];
        }

        if (!Hash::check($request->password, $admin_user->password))
        {
            \Cache::increment('login_error_nums');//累加登录失败次数
            return ['code' => 4004, 'message' => '登录失败', 'log' => "登录密码错误：{$request->username}"];
        }

        $admin_user->last_ip = $request->ip();
        $admin_user->last_at = now();
        $admin_user->save();
//        print_r(json_decode(json_encode($admin_user),true));exit;
//        $admin_user = AdminUserModel::find($admin_user->id);
        $admin_user     = json_decode(json_encode($admin_user), true);
        $this->Lmauth->login($admin_user);

        $redirect_url       = session('fullurl') ? session('fullurl') : '/admin/main/index';
        return ['code' => 0, 'message' => '登录成功.', 'data' => ['adminUser' => $admin_user],'url'=>$redirect_url,];
    }

    function getsession(Request $request)
    {
        var_dump($this->Lmauth->getAdminUser());
    }

    public function logout(Request $request)
    {
        $this->Lmauth->logout();
        return redirect("/admin/login/index");
    }

    function captcha()
    {
        return \Captcha::create('default'); //图片验证码
    }

}
