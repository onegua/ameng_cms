<?php
/**
 * 递归分类所有子id
 * @param $array 所有分类数据
 * @param $id 父类ID
 * @param selfid是否包含自身ID
 * @return array
 */
function get_son_typeids($id,$array=[], $selfid=true)
{
    if(!$array) $array = DB::table('arctype')->get();
    $arr = array();
    foreach($array as $v){
        if($v->reid == $id){
            $arr[] = $v->id;
            $arr = array_merge($arr, get_son_typeids($v->id, $array));
        };
    };
    if($selfid)$arr[] = $id;
    return $arr;
}

/**
 * 根据父ID，递归获取所有下级子ID
 * @param $id
 * @param array $result
 * @param int $step
 * @param int $level
 * @return array
 */
function get_son_typeids_rows($id,$result=[],$step=0, $level=0)
{
    $step++;
    if(!$result) $result = DB::table('arctype')->get();
    $result_new     = array();
    $ii=0;
    foreach($result as $row){
        if($row->reid == $id){
            $ii++;
            $row->step          = $step;
            $row->level         = !$level ? $ii : ($level.'-'.$ii);

            $result_new[]        = json_decode(json_encode($row), true);
            $result_new          = array_merge($result_new, get_son_typeids_rows($row->id, $result, $step, $row->level));
        };
    };
    return $result_new;
}

function layui_tree($top_id=0, $selectd_id=0)
{
    $result = DB::table('arctype')->select(['id','reid as parent_id','typename as title'])->get();
    $data   = json_decode(json_encode($result), true);

    $res = [];
    $tree = [];

    //整理数组
    foreach( $data as $key=>$vo ){
        $select_style       = '';
        if($selectd_id == $vo['id']) $select_style       = 'color:#ff0000;';
        $vo['title_old']    = $vo['title'];
        $vo['title']        = "<span style='{$select_style}'>{$vo['title']}({$vo['id']})</span>";
        $vo['spread']       = true;//节点是否初始展开，默认 false
//        $vo['checked']      = true;
        $res[$vo['id']]     = $vo;
        $res[$vo['id']]['children'] = [];
    }
    unset( $data );

    //查询子孙
    foreach( $res as $key=>$vo ){
        if( $vo['parent_id'] != 0 ){
            if(!$res[$key]) continue;
            $res[$vo['parent_id']]['children'][] = &$res[$key];
        }
    }

    //去除杂质
    foreach( $res as $key=>$vo ){
        if( $vo['parent_id'] == 0 ){
            $tree[] = $vo;
        }
    }
    unset( $res );

    if($top_id)
    {
        foreach ($tree as $tree_row)
        {
            if($tree_row['id'] == $top_id)
            {
                return $tree_row;
                break;
            }
        }
    }else{
        $tree   = [
            'id' =>0,
            'parent_id'=>-1,
            'title' =>'分类',
            'spread' =>1,
            'children' =>$tree
        ];
    }
    return $tree;
}

function get_top_parentid($id, $result= [], $is_top=false){
    $arr=array();
    if(!$result) $result = DB::table('arctype')->get();
    if(!is_array($result)) $result = json_decode(json_encode($result), true);
    foreach($result as $v){
        if($v['id']==$id){
            if($is_top)
            {
                if($v['reid'] == 0) return $v;
                $arr = get_top_parentid($v['reid'],$result,$is_top);
            }else{
                $arr[]=$v;// $arr[$v['id']]=$v['name'];
                $arr=array_merge(get_top_parentid($v['reid'],$result),$arr);
            }
        }
    }return $arr;

}

function myuri()
{
    $uri    = implode(\Request::segments(), "/");
    $uri    = strtolower($uri);
    return $uri;
}

/**
 * 富文本编辑器
 * @param string $body
 * @return mixed|string
 */
function editor($body='')
{
//    return 999;
    $html = <<<'EOF'

<div id="div1">$body</div>
<textarea class="layui-textarea layui-hide" name="body" id="LAY_ditor" lay-verify="details">$body</textarea>
<div class="layui-word-aux st-form-tip"></div>

<script type="text/javascript"src="https://cdn.jsdelivr.net/npm/wangeditor@latest/dist/wangEditor.min.js"></script>
<script>
    //富文本编辑
    const E = window.wangEditor;
    const editor = new E('#div1');
    editor.config.uploadImgHeaders = {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')};//自定义header头
    editor.config.uploadImgServer = '/admin/upload/wangeditor/'; // 配置 server 接口地址
    editor.config.uploadImgMaxSize = 2 * 1024 * 1024; // 2M 限制上传大小
    editor.config.uploadFileName = 'file';//上传文件名
    const $text1 = $('#LAY_ditor');
    editor.config.onchange = function (html) {
        // 第二步，监控变化，同步更新到 textarea
        $text1.val(html);
    };
    editor.create();
    // 第一步，初始化 textarea 的值
    $text1.val(editor.txt.html());
</script>
EOF;
    $html   = str_replace('$body', $body, $html);
    return $html;
}

