<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ArctypeModel extends Model
{

    public $tableComments = '文章分类';
    protected $table = 'arctype';
    protected $guarded = ['id'];//表示在create()方法中不能被赋值的字段

//如何关闭？直接等于 null就行了 https://blog.csdn.net/kina100/article/details/105624803
//    const UPDATED_AT = null;
//    const CREATED_AT = null;
//也可以在类里面直接更改一个变量
    public $timestamps = FALSE;

    /**
     * Validator rules
     * @param type $on
     * @return type
     */
    public function rules()
    {
        return [
            'reid' => ['required', 'integer'],
            'typename' => ['required', 'string', 'max:100'],
            'sortrank' => ['integer'],
            'keywords' => ['nullable', 'string', 'max:255'],
            'description' => ['nullable', 'string', 'max:255'],
        ];
    }

    /**
     * Validator messages
     * @return type
     */
    public function messages()
    {
        return [
        ];
    }

    /**
     * Validator customAttributes
     * @return type
     */
    public function customAttributes()
    {
        return [
            'id' => '分类id',
            'reid' => '上级分类',
            'typename' => '分类名称',
        ];
    }

    public function getAttributeLabel($key)
    {
        $datas = $this->customAttributes();
        return $datas[$key] ?? $key;
    }

    /**
     * Fill the model with an array of attributes.
     * @param type $attributes
     * @return $this
     */
    public function fill($attributes)
    {
        foreach ($attributes as $key => $attribute)
        {
            if ($attribute === null)
            {
                unset($attributes[$key]);
            }
        }
        parent::fill($attributes);
        return $this;
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->orderByDesc('sort');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * 删除分类
     * 1 删除 该ID和所有子ID
     * 2 删除 分类关联的所有文章
     * @param array $model
     */
    function delete($type_id=0)
    {
//        print_r($model->delete());
//        echo 666;exit;
        $type_ids   = get_son_typeids( $type_id);

        $return     = 0;
        //事务BEGIN
        DB::beginTransaction();
        try {
            //1 删除 该ID和所有子ID
            $is_delete  = DB::table('arctype')->whereIn('id',$type_ids)->delete();

            //2 删除 分类关联的所有文章
//            $aids   = DB::table('archives')->whereIn('typeid',$type_ids)->get();
//            DB::table('archives')->whereIn('id',$aids)->delete();
//            DB::table('addonarticle')->where(['aid'=>$aids])->delete();

            DB::commit();//必须放在最后一行。commit后不能回滚，在一个事务中，rollback和commit都代表结束一个事务。要么回滚要么提交。他们是在一个等级上的命令
            if($is_delete) $return = 1;
        }catch(\Exception $e){//注，要么代码在最开头使用 use \Exception,要么使用catch (\Exception $e)
//            var_dump(666);exit;
            DB::rollBack();
        }
        //事务END

        return $return;
    }

}
