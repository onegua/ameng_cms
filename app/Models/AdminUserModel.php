<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUserModel extends Authenticatable
{
    public $tableComments = '用户管理';
    protected $table = 'admin_user';
    protected $guarded = ['id'];//表示在create()方法中不能被赋值的字段

    public $timestamps = FALSE;
}


