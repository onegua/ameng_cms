<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminRoleModel extends Model
{
    public $tableComments = '管理员角色';
    protected $table = 'admin_role';
    protected $guarded = ['id'];//表示在create()方法中不能被赋值的字段

}
