<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class ArchivesModel extends Model
{
    use HasFactory,Searchable;

    protected $table = 'archives';

    /**
     * 可以注入的字段
     * @var string[]
     */
    protected $fillable = [
        'title','description'
    ];

    /**
     * 定义索引的类型
     * @return string
     */
    public function searchableAs(): string
    {
        return 'doc';
    }

    /**
     * 定义需要做搜索的字段
     * @return array
     */
    public function toSearchableArray(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description
        ];
    }
}
