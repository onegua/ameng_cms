<?php
require 'vendor/autoload.php';

use Elasticsearch\ClientBuilder;

$client = ClientBuilder::create()->setHosts(['127.0.0.1'])->build();

    //1 新建索引 index_lumeng
    function create_index($client)
    {
        $params = [
            'index' => 'index_lumeng', #index的名字不能是大写和下划线开头
            'body' => [
                'settings' => [
                    'number_of_shards' => 2,
                    'number_of_replicas' => 0
                ]
            ]
        ];
        $client->indices()->create($params);
    }

    //2 向该索引新增数据
    function add_index($client, $params=[])
    {
        if(!$params)
        {
            $params = [
                'index' => 'index_lumeng',
                'type' => 'mytype',
                //'id' => 1, #可以手动指定id，也可以不指定随机生成
                'body' => [
                    'id'=>'1',
                    'title' => '生命周期概述',
                    'body' => '这个数组中的类在请求被执行前运行，这些 bootstrappers 配置了错误处理，HTTP 内核继承了 Illuminate\Foundation\Http\Kernel 类，该类定义了一个 bootstrappers 数组。 日志， 检测应用环境，以及其它在请求被处理前需要执行的任务。',
                    'first_name' => '张',
                    'last_name' => '三',
                    'age' => 35
                ]
            ];
        }
        $client->index($params);
    }

    //3 获取所有的数据(指定索引名)
    function search_index($client, $index='index_lumeng')
    {
        $params = [
            'index' => $index,
            'type' => 'mytype',
        ];
        $data = $client->search($params);
        return $data;
    }
    //4 获取单个数据
    function search_id($client, $id='5OwR130ByQnxx1Xk3_Y3')
    {
        $params = [
            'index' => 'index_lumeng',
            'type' => 'mytype',
            'id'=>$id,
        ];
        $data = $client->get($params);
    }

    //获取所有数据（所有。。）
    function search_all($client)
    {
        $data = $client->search();
        return $data;
    }

    function search_like($client, $keywords='')
    {
        $index='index_lumeng';
        $json = '{
            "query" : {
                "bool" : {
                  "should" : [
                    { "term" : { "title" : "'.$keywords.'" } },
                    { "term" : { "body" : "'.$keywords.'" } }
                  ],
                }
            }
        }';
        $params = [
            'index' => $index,
            'type' => 'mytype',
            'body' => $json,
        ];
        $data = $client->search($params);
        return $data;
    }

    function search_like_new($client, $keywords='')
    {
        $query = [
            'query' => [
                'bool' => [
                    'should' => [//should 相当于or搜索
                        [ 'match' => [ 'title' => $keywords ] ],
                        [ 'match' => [ 'body' => $keywords ] ],
                    ],
                ]

            ]
        ];

//        $query  = '{
//                    "query": {
//                        "bool": {
//                            "should": [
//                                { "match": { "title": "'.$keywords.'" }},
//                                { "match": { "body":  "'.$keywords.'" }}
//                            ]
//                        }
//                    }
//                }';
        $params = [
            'index' => 'index_lumeng',
            'type' => 'mytype',
//            '_source' => ['title','body'], // 请求指定的字段
            'body' => $query
        ];
//        echo 5;exit;
        $data = $client->search($params);
        return $data;
    }

//    print_r(search_index($client));exit;
    $returndata = search_like_new($client, "请求");
    print_r($returndata);exit;



//$params = [
//    'index' => 'index_lumeng',
//    'type' => 'mytype',
//    //'id' => 1, #可以手动指定id，也可以不指定随机生成
//    'body' => [
//        'id'=>'1',
//        'title' => '服务提供者1',
//        'body' => '内核启动操作中最重要的便是你应用的 服务提供者 了。所有应用下的服务提供者均配置到了 config/app.php 配置文件中的 providers 数组中。 第一步，所有服务提供者的 register 方法会被调用，然后一旦所有服务提供者均注册后， boot 方法才被调用。
//    服务提供者给予框架开启多种多样的组件，像数据库，队列，验证器，以及路由组件。只要被启动服务提供者就可支配框架的所有功能，所以服务提供者也是 Laravel 整个引导周期最重要组成部分。请求调度一旦启动且所有服务提供者被注册，Request 会被递送给路由。路由将会调度请求，交给绑定的路由或控制器，也当然包括路由绑定的中间件。',
//        'first_name' => '张',
//        'last_name' => '三',
//        'age' => 35
//    ]
//];
//    add_index($client, $params);

