/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : amengcms

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-12-19 10:21:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for addonarticle
-- ----------------------------
DROP TABLE IF EXISTS `addonarticle`;
CREATE TABLE `addonarticle` (
  `aid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `typeid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `body` mediumtext,
  PRIMARY KEY (`aid`),
  KEY `typeid` (`typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of addonarticle
-- ----------------------------
INSERT INTO `addonarticle` VALUES ('20', '0', 't3');
INSERT INTO `addonarticle` VALUES ('24', '0', 'a44');
INSERT INTO `addonarticle` VALUES ('29', '0', '<p><img src=\"http://img.baidu.com/hi/jx2/j_0024.gif\"/>&nbsp; &nbsp;</p><p><br/></p><p><br/></p><p>FDSAF<span style=\"text-decoration: underline;\"><em><strong>AFAA</strong></em></span>F</p>');
INSERT INTO `addonarticle` VALUES ('30', '0', '<p>DDD&nbsp;&nbsp;<img src=\"http://img.baidu.com/hi/jx2/j_0028.gif\"/></p><p><br/></p><p><img src=\"/ueditor/php/upload/image/20211130/1638253313306575.jpg\" style=\"\"/></p><p><img src=\"/ueditor/php/upload/image/20211130/1638253304700011.jpg\" style=\"\"/></p><p><br/></p>');
INSERT INTO `addonarticle` VALUES ('31', '0', '<p>111111 333333FDSAF&nbsp;&nbsp;<img src=\"http://img.baidu.com/hi/jx2/j_0004.gif\"/></p><p><img src=\"/uploads/ueditor/image/20211130/1638254557605492.jpg\" alt=\"6.jpg\"/></p><p>FDSAFAS</p>');
INSERT INTO `addonarticle` VALUES ('32', '0', null);
INSERT INTO `addonarticle` VALUES ('57', '0', '<p><img src=\"http://www.amengcms.com/uploads/221209/1670565726418.jpg\" alt=\"C2.jpg\" data-href=\"http%3A%2F%2Fwww.amengcms.com%2Fuploads%2F221209%2F1670565726418.jpg\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>');
INSERT INTO `addonarticle` VALUES ('60', '0', '<p><span style=\"font-size: 14px;\">?</span><span style=\"font-size: 14px;\">?</span><b><u><i>666111111</i></u></b></p><p><b><u><i>55</i></u></b></p><p><br/></p><p>111111<img src=\"http://www.amengcms.com/uploads/221209/1670567881830.png\" alt=\"5.png\" data-href=\"http%3A%2F%2Fwww.amengcms.com%2Fuploads%2F221209%2F1670567881830.png\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>');
INSERT INTO `addonarticle` VALUES ('71', '0', null);
INSERT INTO `addonarticle` VALUES ('72', '0', '<p>动态1212动态1212动态1212动态1212</p>');
INSERT INTO `addonarticle` VALUES ('73', '0', '<p>这里面是内容啊啊啊。</p><p>来个黑乎乎&nbsp;</p><p><img src=\"/uploads/221219/1671416431624.jpg\" alt=\"6.jpg\" data-href=\"%2Fuploads%2F221219%2F1671416431624.jpg\" style=\"max-width:100%;\" contenteditable=\"false\"/></p><p><br/></p><p>结束了。</p>');

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `desc` varchar(255) DEFAULT '' COMMENT '角色描述',
  `permissions` json DEFAULT NULL COMMENT '拥有的权限(route_url)',
  `menu_route_url` text,
  `created_at` datetime DEFAULT NULL COMMENT '添加时间',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uniq_name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of admin_role
-- ----------------------------

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `desc` varchar(255) DEFAULT NULL,
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '登录名',
  `password` varchar(100) DEFAULT NULL COMMENT '登录密码',
  `remember_token` varchar(100) DEFAULT NULL COMMENT '记住登录',
  `name` varchar(100) DEFAULT '' COMMENT '姓名',
  `email` varchar(255) DEFAULT '' COMMENT '邮箱',
  `phone` varchar(255) DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像',
  `introduction` varchar(255) DEFAULT '' COMMENT '介绍',
  `status` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 启用, 2 禁用',
  `last_ip` varchar(255) DEFAULT '' COMMENT '最近一次登录ip',
  `last_at` datetime DEFAULT NULL COMMENT '最近一次登录时间',
  `admin_role_id` smallint(3) DEFAULT NULL COMMENT '角色ID',
  `created_at` datetime DEFAULT NULL COMMENT '添加时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uniq_user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='管理员表';

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('1', null, 'admin', '$2y$10$04FiL6fFq9GLzWsoYdEuP.h8Hwy8AQeLEpCuTbdtEKA3peXZ/zivi', null, 'Admin', 'admin@qq.com', null, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', null, '1', '127.0.0.1', '2022-12-19 01:42:03', '1', '2020-04-13 10:18:10', '2021-12-23 13:09:07');
INSERT INTO `admin_user` VALUES ('19', null, 'demo', '$2y$10$04FiL6fFq9GLzWsoYdEuP.h8Hwy8AQeLEpCuTbdtEKA3peXZ/zivi', null, 'demo', '', '', '', '', '1', '223.84.85.130', '2021-09-28 10:59:49', '2', '2021-08-08 13:51:48', '2021-09-28 10:59:49');
INSERT INTO `admin_user` VALUES ('20', null, 'test1', '$2y$10$ITNbAAakSxPMwSghi0v4lOHUMXRK8xxjtFfBgZ1YururTayoeEiJi', null, '', '', '', '', '', '1', '127.0.0.1', '2021-12-27 09:34:01', '5', '2021-12-15 14:32:58', '2021-12-15 14:33:54');
INSERT INTO `admin_user` VALUES ('22', null, 'test2', '$2y$10$S5AhCKeDUb6ylR4eGdAdXuIY8EHrZc9wzgq2b2l73NLhsbzfRij8q', null, '', '', '', '', '', '1', '', null, '4', '2021-12-15 14:33:43', '2021-12-15 14:33:43');

-- ----------------------------
-- Table structure for archives
-- ----------------------------
DROP TABLE IF EXISTS `archives`;
CREATE TABLE `archives` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `short` int(10) unsigned DEFAULT '0' COMMENT '排序号',
  `title` char(60) NOT NULL DEFAULT '',
  `image` char(100) DEFAULT '',
  `keywords` char(30) DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of archives
-- ----------------------------
INSERT INTO `archives` VALUES ('20', '1', '0', 't33', 'http://www.la8.com/uploads/211129/1638165417988.jpg', '', '介绍\r\n在「日常生活」中 使用任何工具时，如果理解了该工具的工作原理。那么用起来就会更加得心应手。应用开发也是如此，当你能真正懂得一个功能背后实现原理时，用起来会更加顺手，方便。\r\n\r\n文档存在目的是为了让你更加清晰地了解 Laravel 框架是如何工作。通过框架进行全面了解，让一切都不再感觉很「神奇」。相信我，这有助于你更加清楚自己在做什么，对自己想做的事情更加胸有成竹。就算你不明白所有的术语，也不用因此失去信心！只要多一点尝试、学着如何运用，随着你浏览文档的其他部分，你的知识一定会因此增长。', '2021-11-30 01:42:03');
INSERT INTO `archives` VALUES ('24', '1', '0', '可以把该内核想象作一个代表整个应用的大黑盒子，输入 HTTP 请求，返回 HTTP 响应', null, '', 'yyyy', '2021-11-30 02:17:24');
INSERT INTO `archives` VALUES ('27', '1', '0', 'B1', null, '', 'LM', '2021-11-30 06:22:59');
INSERT INTO `archives` VALUES ('28', '3', '0', 'B2', null, '', 'LM', '2021-11-30 06:33:29');
INSERT INTO `archives` VALUES ('29', '2', '0', 'A3', 'http://www.la8.com/uploads/211130/1638253976973.png', '', '生命周期概述\r\n首先\r\nLaravel 应用的所有请求入口都是 public/index.php 文件。而所有的请求都是经由你的 Web 服务器（Apache/Nginx）通过配置引导到这个文件。 index.php 文件代码并不多，但是，这里是加载框架其它部分的起点。', '2021-11-30 06:32:58');
INSERT INTO `archives` VALUES ('30', '1', '0', 'Q1', null, '', 'index.php 文件加载 Composer 生成的自动加载设置，然后从 bootstrap/app.php 脚本中检索 Laravel 应用程序的实例。 Laravel 本身采取的第一个动作是创建一个应用程序 / 服务容器。\r\n\r\nHTTP / Console 内核\r\n接下来， 根据进入应用程序的请求类型来将传入的请求发送到 HTTP 内核或控制台内核。而这两个内核是用来作为所有请求都要通过的中心位置。 现在，我们先看看位于 app/Http/Kernel.php 中的 HTTP 内核。', '2021-11-30 06:25:42');
INSERT INTO `archives` VALUES ('31', '6', '0', 'G1', 'http://www.la8.com/uploads/211130/1638253796885.jpg', '', 'HTTP 内核继承了 Illuminate\\Foundation\\Http\\Kernel 类，该类定义了一个 bootstrappers 数组。 这个数组中的类在请求被执行前运行，这些 bootstrappers 配置了错误处理， 日志， 检测应用环境，以及其它在请求被处理前需要执行的任务。\r\n\r\nHTTP 内核还定义了所有请求被应用程序处理之前必须经过的 HTTP 中间件 ，这些中间件处理 HTTP 会话 读写、判断应用是否处于维护模式、 验证 CSRF 令牌 等等。', '2021-11-30 06:42:41');
INSERT INTO `archives` VALUES ('32', '2', '0', 'test2', null, '', '', '2022-12-05 07:10:00');
INSERT INTO `archives` VALUES ('33', '1', '0', '底层方法 基础支撑', '', '', '', null);
INSERT INTO `archives` VALUES ('34', '1', '0', '页面元素 规范 公共类 属性', '', '', '', null);
INSERT INTO `archives` VALUES ('35', '1', '0', '模块规范 使用 扩展', '', '', '', null);
INSERT INTO `archives` VALUES ('36', '1', '0', '常见问题 FAQ', '', '', '', null);
INSERT INTO `archives` VALUES ('37', '1', '0', '更新日志 changelog', '', '', '', null);
INSERT INTO `archives` VALUES ('38', '2', '0', '布局 栅格 后台管理布局', '', '', '', null);
INSERT INTO `archives` VALUES ('39', '4', '0', '颜色 主色调 颜色搭配', '', '', '', null);
INSERT INTO `archives` VALUES ('40', '4', '0', '图标 iconfont 字体图标', '', '', '', null);
INSERT INTO `archives` VALUES ('41', '4', '0', '动画 内置 CSS3 动画', '', '', '', null);
INSERT INTO `archives` VALUES ('42', '4', '0', '按钮 button', '', '', '', null);
INSERT INTO `archives` VALUES ('43', '4', '0', '表单 form 元素集合', '', '', '', null);
INSERT INTO `archives` VALUES ('44', '4', '0', '导航 nav 面包屑', '', '', '', null);
INSERT INTO `archives` VALUES ('45', '3', '0', '菜单 menu 基础菜单', '', '', '', null);
INSERT INTO `archives` VALUES ('46', '3', '0', '选项卡 Tabs 切换', '', '', '', null);
INSERT INTO `archives` VALUES ('47', '3', '0', '进度条 progress', '', '', '', null);
INSERT INTO `archives` VALUES ('48', '3', '0', '面板 panel 卡片 折叠', '', '', '', null);
INSERT INTO `archives` VALUES ('49', '3', '0', '表格 静态 table', '', '', '', null);
INSERT INTO `archives` VALUES ('50', '3', '0', '徽章 小圆点 小边框', '', '', '', null);
INSERT INTO `archives` VALUES ('51', '4', '0', '时间线 timeline', '', '', '', null);
INSERT INTO `archives` VALUES ('52', '4', '0', '辅助 引用 字段集 横线等', '', '', '', null);
INSERT INTO `archives` VALUES ('53', '4', '0', '弹出层 layer', '', '', '', null);
INSERT INTO `archives` VALUES ('54', '4', '0', '日期与时间选择 laydate', '', '', '', null);
INSERT INTO `archives` VALUES ('55', '4', '0', '分页 laypage', '', '', '', null);
INSERT INTO `archives` VALUES ('56', '5', '0', '模板引擎 laytpl', '', '', '', null);
INSERT INTO `archives` VALUES ('57', '5', '0', '数据表格', '/uploads/221209/1670565735853.png', '', '', '2022-12-09 06:02:16');
INSERT INTO `archives` VALUES ('58', '5', '0', '表单 form111', '', '', '', null);
INSERT INTO `archives` VALUES ('59', '5', '0', '文件上传 uploa', '', '', '', null);
INSERT INTO `archives` VALUES ('60', '6', '0', '下拉菜单 dropdown', '/uploads/221209/1670564341817.png', '', '', '2022-12-09 06:38:11');
INSERT INTO `archives` VALUES ('61', '6', '0', '穿梭框 transfer', '', '', '', null);
INSERT INTO `archives` VALUES ('62', '6', '0', '树形组件 tree', '', '', '', null);
INSERT INTO `archives` VALUES ('63', '6', '0', '颜色选择器 colorpicker', '', '', '', null);
INSERT INTO `archives` VALUES ('64', '7', '0', '常用元素操作 element', '', '', '', null);
INSERT INTO `archives` VALUES ('65', '7', '0', '滑块 slider', '', '', '', null);
INSERT INTO `archives` VALUES ('66', '7', '0', '评分 rate', '', '', '', null);
INSERT INTO `archives` VALUES ('67', '8', '0', '轮播 carousel', '', '', '', null);
INSERT INTO `archives` VALUES ('68', '8', '0', '流加载 flow', '', '', '', null);
INSERT INTO `archives` VALUES ('69', '9', '0', '工具组件 util', '', '', '', null);
INSERT INTO `archives` VALUES ('70', '9', '0', '代码高亮显示 code', '', '', '', null);
INSERT INTO `archives` VALUES ('71', '10', '0', '开始使用 - 入门指南', null, '', '', '2022-12-19 02:17:30');
INSERT INTO `archives` VALUES ('72', '5', '0', '动态1212', null, '', '', '2022-12-12 07:16:13');
INSERT INTO `archives` VALUES ('73', '10', '3333', '这个是第1个标题', '/uploads/221219/1671416404552.png', '', '', '2022-12-19 02:20:35');

-- ----------------------------
-- Table structure for arctype
-- ----------------------------
DROP TABLE IF EXISTS `arctype`;
CREATE TABLE `arctype` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `reid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `image` varchar(255) DEFAULT NULL,
  `sortrank` smallint(5) unsigned NOT NULL DEFAULT '50' COMMENT '排序',
  `typename` char(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `description` char(150) NOT NULL DEFAULT '' COMMENT '描述',
  `keywords` varchar(60) NOT NULL DEFAULT '' COMMENT '关键词',
  `seotitle` varchar(80) NOT NULL DEFAULT '' COMMENT 'seo标题',
  `body` text COMMENT '单页内容',
  `created_time` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`),
  KEY `reid` (`reid`),
  KEY `sortrank` (`sortrank`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of arctype
-- ----------------------------
INSERT INTO `arctype` VALUES ('1', '0', null, '50', '关于我们', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('2', '0', '/uploads/221212/1670810922950.png', '50', '资讯', '', '', '', '<p>3333<img src=\"/uploads/221212/1670811111311.jpg\" alt=\"6.jpg\" data-href=\"%2Fuploads%2F221212%2F1670811111311.jpg\" contenteditable=\"false\" style=\"font-size: 14px; max-width: 100%;\"/></p><p></p>', null);
INSERT INTO `arctype` VALUES ('3', '0', null, '50', '产品', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('4', '0', '/uploads/221209/1670568152256.jpg', '50', '新闻', '', '', '', '<p>333</p><p><img src=\"http://www.amengcms.com/uploads/221209/1670568086567.png\" alt=\"KF1.png\" data-href=\"http%3A%2F%2Fwww.amengcms.com%2Fuploads%2F221209%2F1670568086567.png\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', null);
INSERT INTO `arctype` VALUES ('5', '2', null, '50', '动态', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('6', '3', null, '50', '分类一', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('7', '3', null, '50', '分类二', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('8', '6', null, '50', '分类一1', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('9', '6', null, '50', '分类一2', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('10', '9', null, '50', '分类一2-1', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('12', '6', null, '50', '666', '', '', '', null, null);
INSERT INTO `arctype` VALUES ('13', '6', null, '50', '6667', '', 'keywords', 'seotitle', null, null);
INSERT INTO `arctype` VALUES ('14', '6', null, '50', '66696', '', 'keywords', 'seotitle6', null, '2021-12-09 07:06:06');
INSERT INTO `arctype` VALUES ('23', '6', null, '50', 'a2', '', 'keywords', 'seotitle6', null, null);
INSERT INTO `arctype` VALUES ('24', '10', null, '0', '2-1-1', '', '', '', null, null);
